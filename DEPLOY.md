### Local con docker

> Ejectuar las tareas con sudo o root.

Crear directorio de trabajo si no existe:
``` bash
mkdir /opt/docker/
```

Clonar proyecto:
``` bash
sudo git clone https://gitlab.com/urdanetarj/beeping.git /opt/docker/beeping 
```


### En caso de querer hacer el deploy manual, se deben realizar estos pasos:
* Setea el nombre del proyecto dentro del /etc/hosts (ej: 127.0.0.1 beeping.local )
* cp -a src/.env.local .env
* Levanta el contenedor con docker-compose -f docker-compose-local.yml up -d


Acceder:

> * beeping: http://beeping.local:8150

---


### Cambio de puerto **(Opcional)**

En caso de querer cambiar el port se debe editar los archivos ports.conf y 000-default.conf y cambiar la linea:

```
cd  /opt/docker/beeping/docker-files/config
cp -a 000-default.conf.bkp 000-default.conf
cp -a ports-local.conf.bkp ports.conf

#Editar los files

#Actual ports.conf
Listen 80

#Cambio:
Listen 90

#Actual 000-default.conf
<VirtualHost *:80>

#Cambio 
<VirtualHost *:90>
```

Reiniciar contenedor para que tome los cambios:

 ```bash
cd /opt/docker/beeping 

sudo docker-compose -f docker-compose-local.yml stop

sudo docker-compose -f docker-compose-local.yml up -d
 ```

> * beeping: http://beeping.local:"PUERTONUEVO"

---


### Local sin docker

> Ejectuar las tareas con sudo o root.

Crear directorio de trabajo si no existe:
``` bash
mkdir /opt/docker/
```

Clonar proyecto:
``` bash
sudo git clone https://gitlab.com/urdanetarj/beeping.git /opt/docker/beeping 
```

### En caso de querer hacer el deploy manual, se deben realizar estos pasos:
* cp -a src/.env.local .env
* Reemplazar las variables de conexion de tu bases de datos
  DB_CONNECTION=mysql
  DB_HOST=127.0.0.1
  DB_PORT=3306
  DB_DATABASE=beeping
  DB_USERNAME=root
  DB_PASSWORD=12345
* crear una base de datos de nombre (beeping) o el nombre que quieras
* composer install (composer previamente instalado)
* php artisan migrate:fresh --seed (para correr las migraciones)
* php artisan command:totalOrders (cron que calcula el total de todas las ordenes de la DB)
* php artisan serve