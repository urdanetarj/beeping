<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Order;
class Orders extends Component
{
    use WithPagination;

    public function render()
    {
        $orders=Order::select('orders.*','ol.qty')->join('orders_lines as ol','ol.order_id','=','orders.id')->orderBy('id','desc')->paginate(10);
        return view('livewire.orders',compact('orders'));
    }
}
