<?php

namespace App\Console\Commands;

use App\Services\TotalOrdersService;
use Illuminate\Console\Command;

class TotalOrdersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:totalOrders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calcule el coste total de todas las órdenes de la DB, para calcular este coste hay que multiplicar el order_line “qty” por el “product cost';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        (new TotalOrdersService())->process();
    }
}
