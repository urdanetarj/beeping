<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderLine extends Model
{
    use HasFactory;

    protected $table='orders_lines';


    /**
     * @return mixed
     */
    public static function obtenerTotalOrder(){
        return OrderLine::select('o.id as ordersId',DB::raw('sum(orders_lines.qty * p.cost) as total'))
            ->join('orders as o','o.id','=','orders_lines.order_id')
            ->join('products as p','p.id','=','orders_lines.product_id')
            ->groupBy('orders_lines.order_id')
            ->get();
    }
}
