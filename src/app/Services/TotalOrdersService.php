<?php

namespace App\Services;

use App\Models\OrderLine;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class TotalOrdersService
{
        public function process(){
            Log::channel('totalOrders')->info("Iniciar Proceso TotalOrdersCommand ".Carbon::now().PHP_EOL);
            echo 'Iniciar Proceso TotalOrdersCommand '.Carbon::now().PHP_EOL;

            $totalOrdenes=OrderLine::obtenerTotalOrder();

            if(!$totalOrdenes->isEmpty()){
                foreach ($totalOrdenes as $totalOrden){
                    Log::channel('totalOrders')->info("Orders#:".$totalOrden->ordersId."|Total orden:".$totalOrden->total);
                    echo "Orders#:".$totalOrden->ordersId."|Total orden:".$totalOrden->total.PHP_EOL;
                }
            }else{
                Log::channel('totalOrders')->info("No se encontraron ordenes a procesar");
                echo 'No se encontraron ordenes a procesar';
            }


            Log::channel('totalOrders')->info("Final Proceso TotalOrdersCommand ".Carbon::now().PHP_EOL);
            echo 'Final Proceso TotalOrdersCommand '.Carbon::now().PHP_EOL;
        }
}
