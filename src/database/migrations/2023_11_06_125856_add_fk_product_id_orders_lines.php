<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkProductIdOrdersLines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_lines', function (Blueprint $table) {
            $table->foreign('product_id', 'fk_product_id_orders_lines')->references('id')->on('products')->onDelete('restrict')->onUpdate('cascade');
            $table->charset='utf8mb4';
            $table->collation='utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_lines', function (Blueprint $table) {
            $table->dropforeign('order_id');
        });
    }
}
