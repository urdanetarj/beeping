# beeping

## Comenzando :rocket:

_El proyecto tiene  branchs activas correspondientes a cada ambiente:_

* [main](https://gitlab.com/urdanetarj/beeping/-/tree/main?ref_type=heads) - Branch de producción.



## Pre-requisitos :clipboard:

_Requisitos necesarios para el funcionamiento de la aplicación_

- Docker
- docker-compose
- git



## Arquitectura :wrench:
_Descripción de los recursos asignados a la vm_

- Debian 11.1
- 4 vCPU
- 4GB vRAM
- 30 GB Disk

### Diagrama:



## Construido con :hammer_and_wrench:

_Detalle de las soluciones que se utilizaron en el proyecto_

* Php v7.4 & Apache 
* Mysql v8.0.28
* Bind 9.16.25 (Alpine)


## Despliegue :docker:

* [Acceder](https://gitlab.com/urdanetarj/beeping/-/blob/main/DEPLOY.md?ref_type=heads) - Detalle para desplegar el proyecto en docker.




