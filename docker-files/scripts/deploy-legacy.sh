CI_PROJECT_NAME=$1 # Ejemplo: sigat
DOCKERCOMPOSE=$2

cd /opt/docker/$CI_PROJECT_NAME
docker-compose -f $DOCKERCOMPOSE stop 
docker-compose -f $DOCKERCOMPOSE pull 
docker-compose -f $DOCKERCOMPOSE up -d 