#!/bin/bash
CI_PROJECT_NAME=$1 

sudo chown -R ansible.root /opt/docker/$CI_PROJECT_NAME/
cd /opt/docker/$CI_PROJECT_NAME/ && git reset --hard && git pull
sudo chown -R ansible.root /opt/docker/$CI_PROJECT_NAME/