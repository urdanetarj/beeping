#!/bin/bash

# Ejecuto el composer install en raiz
composer install --working-dir=$APACHE_DOCUMENT_ROOT

#Permisos carpeta de logs:
chmod 777 $APACHE_DOCUMENT_ROOT/logs
chmod -R 777 $APACHE_DOCUMENT_ROOT/storage

# Levanto el webserver.
apache2ctl -D FOREGROUND
