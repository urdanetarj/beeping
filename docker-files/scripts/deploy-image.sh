#!/bin/bash

CI_PROJECT_NAME=$1 
CI_COMMIT_BRANCH=$2


if [[ $CI_COMMIT_BRANCH == preproduccion ]]; then
   DOCKERCOMPOSE=docker-compose-preprod.yml
elif [[ $CI_COMMIT_BRANCH == master ]]; then
   DOCKERCOMPOSE=docker-compose.yml
elif [[ $CI_COMMIT_BRANCH == qa-testing ]]; then
   DOCKERCOMPOSE=docker-compose-qa.yml
else
   echo "El nombre de la branch no es master ni preproduccion ni qa."
   exit
fi

#Bajar contenedor
echo "Bajando container $CI_PROJECT_NAME"
cd /opt/docker/$CI_PROJECT_NAME && sudo docker-compose -f $DOCKERCOMPOSE stop $CI_PROJECT_NAME

#Haciendo pull de la imagen.
cd /opt/docker/$CI_PROJECT_NAME && sudo docker-compose -f $DOCKERCOMPOSE pull $CI_PROJECT_NAME

#Levantando contenedor.
cd /opt/docker/$CI_PROJECT_NAME && sudo docker-compose -f $DOCKERCOMPOSE up -d $CI_PROJECT_NAME