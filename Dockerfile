FROM php:7.4-apache

ENV TZ="America/Argentina/Buenos_Aires"

ARG AMBIENTE

ENV APACHE_DOCUMENT_ROOT /var/www/beeping

RUN apt-get update && apt-get install -y libpq-dev \
     unzip \
     nodejs \
     openssl \
     libssl-dev \
     libcurl4-openssl-dev \
     libpng-* \
     libldb-dev \
     libldap2-dev \
     libxml2-dev \
     php-xdebug* \
     php-imagick* \
     php-redis* \
     php-pear* \
     php-dev* \
     cron \
     git \
     libzip-dev \
     supervisor \
     libaio1 \
     && rm -rf /var/lib/apt/lists/*


RUN docker-php-ext-install calendar exif gettext mysqli pdo pdo_mysql gd soap zip
RUN docker-php-ext-enable calendar exif gettext mysqli pdo_mysql gd soap zip
ENV APACHE_DOCUMENT_ROOT /var/www/beeping

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN curl -sS https://getcomposer.org/installer | php -- \
  --install-dir=/usr/bin --filename=composer

WORKDIR ${APACHE_DOCUMENT_ROOT}

ADD src/ ${APACHE_DOCUMENT_ROOT}

RUN if [ "$AMBIENTE" = "produccion" ] ; then cp -a .env.produccion .env; elif [ "$AMBIENTE" = "preproduccion" ]; then  cp -a .env.preprod .env; elif [ "$AMBIENTE" = "qa" ]; then  cp -a .env.qa .env; fi
RUN if [ "$AMBIENTE" = "produccion" ] ; then cp -a config/database.php.prd config/database.php ; elif [ "$AMBIENTE" = "preproduccion" ]; then  cp -a config/database.php.preprod config/database.php ; elif [ "$AMBIENTE" = "qa" ]; then  cp -a config/database.php.qa config/database.php; fi

EXPOSE 80

ADD ./docker-files/config/000-default.conf /etc/apache2/sites-available/
ADD ./docker-files/config/apache2.conf /etc/apache2/
ADD docker-files/config/ports.conf /etc/apache2/
RUN chmod -R 777 ${APACHE_DOCUMENT_ROOT}/storage
ADD ./docker-files/config/php.ini /usr/local/etc/php/php.ini
RUN a2enmod rewrite

ADD ./docker-files/scripts/entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
